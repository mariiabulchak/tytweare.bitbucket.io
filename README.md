Every enterprise is creating the team when it need to apply the creative ideas, to solve tasks or to
complete the project. The main task of the all team members is to work together as much as possible
and get a positive result from this interaction. Therefore, the main task of the team leader is to rally the
team around him and around a common goal. In this situation, the most important question is how to
motivate team members, because motivation can inspire, encourage and stimulate people to achieve
the great success. Motivation can also create an environment that will promote effective collective work
to achieve common goals or objectives.
The aim of the article is the development of theoretical positions and practical recommendations for
process of motivational teamwork in the company, determine the components involved in the motivational process and identify the basic mechanisms of the work stimulating.
In the article defined the features of motivational team process and highlighted the main stages of
motivation. It determined the main methods of motivation as the "carrot and stick", system of sanctions and incentives, and psychological methods. Generalized approaches and principles of the team
motivation. The motivation process should viewed from two sides, applying the principles of synthesis
and analysis. It is necessary to provide three basic components in the motivation, including individual
motivation of each team member, project leader motivation and motivation of the team as a whole.
Important feature of the encouraging individual team members based on a hierarchy of human needs
Maslow. Motivation of the project leader must depends from the outcome of the project. The whole
team need motivate through creating a sense of reliability and sociability. An important element that has
a positive effect on the functioning of the team is supportive and creative environment, which needs to
pay special attention.